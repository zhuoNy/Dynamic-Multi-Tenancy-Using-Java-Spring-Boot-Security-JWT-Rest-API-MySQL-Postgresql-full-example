package com.amran.dynamic.multitenant.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NotFoundException extends HttpException {

	private static final long serialVersionUID = -4250632442486883479L;

	public NotFoundException(Integer code) {
		this.setCode(code);
	}

	public NotFoundException(String message) {
		this.setMessage(message);
	}
}
