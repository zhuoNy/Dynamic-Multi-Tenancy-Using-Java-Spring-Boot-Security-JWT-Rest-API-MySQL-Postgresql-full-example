package com.amran.dynamic.multitenant.tenant.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.amran.dynamic.multitenant.common.BaseEntity;
import com.amran.dynamic.multitenant.enums.ProductStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Md. Amran Hossain
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tbl_product")
public class Product extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;

	private String subtitle;

	private String sku;

	private String quantity;
	@Enumerated(EnumType.STRING)
	private ProductStatus status;

	private String price;

	private String discountPrice;

	private String description;

	private String tags;

	@OneToMany(mappedBy = "productId", fetch = FetchType.EAGER)
	private List<ProductImg> images;

}
