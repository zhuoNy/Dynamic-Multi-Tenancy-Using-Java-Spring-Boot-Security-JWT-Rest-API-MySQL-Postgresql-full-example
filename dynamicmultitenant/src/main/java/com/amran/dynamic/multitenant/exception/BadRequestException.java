package com.amran.dynamic.multitenant.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BadRequestException extends HttpException {

	private static final long serialVersionUID = -4250632442486883479L;

	public BadRequestException(Integer code) {
		this.setCode(code);
	}

	public BadRequestException(String message) {
		this.setMessage(message);
	}
}
