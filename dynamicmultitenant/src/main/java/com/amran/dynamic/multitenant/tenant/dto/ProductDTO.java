package com.amran.dynamic.multitenant.tenant.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO {
	/**
	 * 产品 ID 号 id = 0 时表示新增，id > 0 时表增更新
	 */
	private Long id;
	/**
	 * 名称
	 */
	@NotBlank
	private String title;
	/**
	 * 子标题
	 */
	private String subtitle;
	/**
	 * 货号
	 */
	private String sku;
	/**
	 * 总数量
	 */
	private Integer quantity;
	/**
	 * 类型
	 */
	private String productType;
	/**
	 * 状态 ACTIVE, DRAFT
	 */
	private String status;
	/**
	 * 原价
	 */
	private String price;
	/**
	 * 销售价
	 */
	private String discountPrice;
	/**
	 * 说明
	 */
	private String description;
	/**
	 * 标签
	 */
	private String tags;
	/**
	 * 图片
	 */
	private List<String> images;
	/**
	 * 多属性
	 */
	private List<String> options;
	/**
	 * sku variants
	 */
	private List<SkuDTO> variants;
}
