package com.amran.dynamic.multitenant.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class HttpException extends RuntimeException {

	private static final long serialVersionUID = 2088195098320685602L;
	private Integer code;
	private String message;

}
