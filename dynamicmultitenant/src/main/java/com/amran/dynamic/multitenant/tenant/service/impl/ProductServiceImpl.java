package com.amran.dynamic.multitenant.tenant.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.amran.dynamic.multitenant.tenant.dto.ProductDTO;
import com.amran.dynamic.multitenant.tenant.entity.Product;
import com.amran.dynamic.multitenant.tenant.repository.ProductRepository;
import com.amran.dynamic.multitenant.tenant.service.ProductService;

/**
 * @author Md. Amran Hossain
 */
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public Page<Product> query(Pageable pageable) {
		return productRepository.findAll(pageable);
	}

	@Override
	public Product save(ProductDTO dto) {
		return null;
	}

	@Override
	public Optional<Product> getOne(Long id) {
		return productRepository.findById(id);
	}
}
