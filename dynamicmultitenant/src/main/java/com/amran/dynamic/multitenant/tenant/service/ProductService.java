package com.amran.dynamic.multitenant.tenant.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.amran.dynamic.multitenant.tenant.dto.ProductDTO;
import com.amran.dynamic.multitenant.tenant.entity.Product;

/**
 * @author Md. Amran Hossain
 */
public interface ProductService {

	Product save(ProductDTO dto);

	Optional<Product> getOne(Long id);

	Page<Product> query(Pageable pageable);

}
