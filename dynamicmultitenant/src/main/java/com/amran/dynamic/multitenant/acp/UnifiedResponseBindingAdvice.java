package com.amran.dynamic.multitenant.acp;

import java.util.Objects;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.amran.dynamic.multitenant.anotation.UnifiedResponseBinding;
import com.amran.dynamic.multitenant.support.UnifyResponse;
import com.amran.dynamic.multitenant.util.HttpRequestUtil;

@ControllerAdvice
public class UnifiedResponseBindingAdvice implements ResponseBodyAdvice<Object> {

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return returnType.getContainingClass().isAnnotationPresent(RestController.class)
				&& Objects.requireNonNull(returnType.getMethod()).isAnnotationPresent(UnifiedResponseBinding.class);
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		UnifiedResponseBinding responseBinding = Objects.requireNonNull(returnType.getMethod())
				.getAnnotation(UnifiedResponseBinding.class);
		Integer code = responseBinding.code();
		String message = responseBinding.message();
		String req = HttpRequestUtil.getSimpleMessage(request);
		return new UnifyResponse<Object>(code, message, req, body);
	}

}
