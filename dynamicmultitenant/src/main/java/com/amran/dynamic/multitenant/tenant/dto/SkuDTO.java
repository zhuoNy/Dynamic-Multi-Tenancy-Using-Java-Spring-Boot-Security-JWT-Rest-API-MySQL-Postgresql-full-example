package com.amran.dynamic.multitenant.tenant.dto;

import java.util.List;

import com.amran.dynamic.multitenant.enums.InventoryPolicy;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * created by ny.z on 2021/05/02 10:40:26
 */
@Getter
@Setter
public class SkuDTO {

	private Long id;
	/**
	 * 对应的单个图片uri
	 */
	private String image;
	/**
	 * 可用的数量
	 */
	private String availableQuantity;
	/**
	 * 对应属性的值
	 */
	private List<String> options;
	/**
	 * 原价
	 */
	private String compareAtPrice;
	/**
	 * 销售价
	 */
	private String price;
	/**
	 * 货号
	 */
	private String sku;
	/**
	 * 对应库存销售策略 {@link InventoryPolicy}
	 * 
	 * DENY, CONTINUE
	 */
	private String inventoryPolicy;

}
