package com.amran.dynamic.multitenant.controller;

import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amran.dynamic.multitenant.anotation.UnifiedResponseBinding;
import com.amran.dynamic.multitenant.exception.NotFoundException;
import com.amran.dynamic.multitenant.security.RequestAuthorization;
import com.amran.dynamic.multitenant.tenant.dto.ProductDTO;
import com.amran.dynamic.multitenant.tenant.entity.Product;
import com.amran.dynamic.multitenant.tenant.service.ProductService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author ny
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/product")
@RequestAuthorization
public class ProductController {

	@Autowired
	ProductService productService;

	@PostMapping("/doEdit")
	@UnifiedResponseBinding
	public ProductDTO save(@RequestBody @Validated ProductDTO dto) {
		log.info("call product save method...");
		return dto;
	}

	@GetMapping("/page")
	@UnifiedResponseBinding
	public Page<Product> getAllProduct() {
		Pageable pageable = PageRequest.of(0, 10);
		return productService.query(pageable);
	}

	@GetMapping("/get")
	@UnifiedResponseBinding
	public Product getOne(@RequestParam @Positive Long id) {
		Product product = productService.getOne(id).orElseThrow(() -> new NotFoundException(40000));
		return product;
	}

}
