/*
 Navicat MySQL Data Transfer

 Source Server         : db.server.com
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : db.server.com:3306
 Source Schema         : master_db

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 30/04/2021 15:38:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_tenant_master
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tenant_master`;
CREATE TABLE `tbl_tenant_master`  (
  `tenant_client_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `db_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `driver_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tenant_client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_tenant_master
-- ----------------------------
INSERT INTO `tbl_tenant_master` VALUES (100, 'tenant1', 'jdbc:mysql://db.server.com:3306/tenant1?useSSL=false', 'root', '123456', 'com.mysql.cj.jdbc.Driver', 'ACTIVE');
INSERT INTO `tbl_tenant_master` VALUES (200, 'tenant2', 'jdbc:mysql://db.server.com:3306/tenant2?useSSL=false', 'root', '123456', 'com.mysql.cj.jdbc.Driver', 'ACTIVE');

SET FOREIGN_KEY_CHECKS = 1;
