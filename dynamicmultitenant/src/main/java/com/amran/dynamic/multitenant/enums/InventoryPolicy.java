package com.amran.dynamic.multitenant.enums;

public enum InventoryPolicy {
	DENY, CONTINUE;
}
