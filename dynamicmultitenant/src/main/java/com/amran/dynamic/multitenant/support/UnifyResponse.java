package com.amran.dynamic.multitenant.support;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UnifyResponse<T> {
	public UnifyResponse(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public UnifyResponse(int code, String message, String request) {
		this.code = code;
		this.message = message;
		this.request = request;
	}

	private Integer code;
	@JsonProperty(value = "msg")
	private String message;
	private String request;
	private T data;

}
