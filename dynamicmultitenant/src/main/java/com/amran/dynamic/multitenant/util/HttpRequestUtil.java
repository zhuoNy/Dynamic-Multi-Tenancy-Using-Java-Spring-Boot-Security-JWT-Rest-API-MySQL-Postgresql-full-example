package com.amran.dynamic.multitenant.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.server.ServerHttpRequest;

public class HttpRequestUtil {
	public static String getSimpleMessage(HttpServletRequest request) {
		return String.format("%s %s", request.getMethod(), request.getRequestURI());
	}

	public static String getSimpleMessage(ServerHttpRequest request) {
		return String.format("%s %s", request.getMethod(), request.getURI().getPath());
	}

	public static Pageable covertToPageable(Integer pageNo, Integer pageSize) {
		return PageRequest.of(pageNo > 0 ? pageNo - 1 : pageNo, pageSize);
	}
}
