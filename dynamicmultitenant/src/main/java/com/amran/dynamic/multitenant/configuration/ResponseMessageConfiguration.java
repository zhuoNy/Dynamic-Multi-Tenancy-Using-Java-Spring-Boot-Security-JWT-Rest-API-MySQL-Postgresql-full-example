package com.amran.dynamic.multitenant.configuration;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@PropertySource(value = "classpath:config/response-code.properties", encoding = "UTF-8")
@ConfigurationProperties(prefix = "message")
public class ResponseMessageConfiguration {
	private Map<Integer, String> codes = new HashMap<>();

	public String message(Integer code) {
		return codes.get(code);
	}
}
